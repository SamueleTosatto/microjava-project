package ssw.mj;

/**
 * Thrown if the interpreter runs into an illegal state.
 */
public class InterpreterException extends RuntimeException {
	private static final long serialVersionUID = 3168895954175369104L;

	/**
	 * Indicates that a trap opcode has been encountered by the interpreter.
	 */
	public static final class TrapException extends InterpreterException {
		private static final long serialVersionUID = 2316137465800026465L;

		public TrapException(byte next) {
			super("trap(" + next + ")");
		}
	}

	/**
	 * Thrown by the interpreter if the degree of the root is non-positive.
	 */
	public static final class NegativeExponentException extends
			InterpreterException {

		private static final long serialVersionUID = -1267501582964429304L;

		public NegativeExponentException(int n) {
			super("exponent must not be negaitive: " + n);
		}
	}

	/**
	 * Thrown by the interpreter if the degree of the root is non-positive.
	 */
	public static final class NonPositiveRootDegreeException extends
			InterpreterException {
		private static final long serialVersionUID = -3978404784615072030L;

		public NonPositiveRootDegreeException(int n) {
			super("degree must be positive: " + n);
		}
	}

	public InterpreterException() {
		super();
	}

	public InterpreterException(String s) {
		super(s);
	}

}
