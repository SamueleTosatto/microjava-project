package ssw.mj.codegen;

import java.util.ArrayList;

/**
 * MircoJava Jump Label. Forward jumps use labels which are still undefined.
 * Such jump instructions are linked in the code. A fixup is made as soon as the
 * label becomes defined. Backward jumps use labels which are already defined.
 * Such jump instructions are already generated with the definite label address.
 */
public class Label {
	
		private int adr;
		private ArrayList<Integer> fixupList;
		private Code code;
		
		public Label(Code code){
			adr=-1;
			fixupList = new ArrayList<Integer>();
			this.code=code;
		}
		/**
		 * Defines the places FROM WHERE you want to jump
		 */
		public void putAdr(){
			if(adr>=0)
				code.put2(adr -(code.pc-1));
			else{
				fixupList.add(code.pc);
				code.put2(0);
			}
		}
		/**
		 * Defines the place WHERE you want to jump to
		 */
		public void here(){
			if(adr>=0);//TODO throw new Exception("Label defined twice");
			for(int pos: fixupList){
				code.put2(pos , code.pc-(pos-1));
			}
			adr=code.pc;
		}

}
