package ssw.mj.symtab;

import java.util.Collections;
import java.util.Map;

/**
 * MicroJava Type Structures: A type structure stores the type attributes of a
 * declared type.
 */
public class Struct {
	/** Possible codes for structure kinds. */
	public enum Kind {
		None, Int, Char, Arr, Class
	}

	/** Kind of the structure node. */
	public final Kind kind;
	/** Only for Arr: Type of the array elements. */
	public final Struct elemType;
	/** Only for Class: First element of the linked list of local variables. */
	public Map<String, Obj> fields = Collections.emptyMap();

	public Struct(Kind kind, Struct elemType) {
		this.kind = kind;
		this.elemType = elemType;
	}

	public Struct(Kind kind) {
		this(kind, null);
	}

	/**
	 * Creates a new array structure with a specified element type.
	 */
	public Struct(Struct elemType) {
		this(Kind.Arr, elemType);
	}

	/**
	 * Retrieves the field <code>name</code>.
	 */
	public Obj findField(String name) {
		return fields.get(name);
	}

	/** Only for Class: Number of fields. */
	public int nrFields() {
		return fields.size();
	}

	public boolean isRefType() {
		 return kind == Kind.Class || kind == Kind.Arr;
	}
	
	boolean equals(Struct other) {
		 if (kind == Kind.Arr) {
			 return other.kind == Kind.Arr && elemType.equals(other.elemType);
		 } else {
			 return this == other; // must be same type node
		 }
	}
	
	public boolean compatibleWith(Struct other) {
		 return this.equals(other) ||
		 (this == Tab.nullType && other.isRefType()) ||
		 (other == Tab.nullType && this.isRefType());
	}
	
	public boolean assignableTo(Struct dest) {
		 return this.equals(dest) ||
		 (this == Tab.nullType && dest.isRefType()) ||
		 (this.kind == Kind.Arr && dest.kind == Kind.Arr &&
		 dest.elemType == Tab.noType); // for function len()
	}
}
