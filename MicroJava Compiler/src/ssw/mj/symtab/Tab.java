// TODO KF: 0/24
// TODO KF: not terminating JUnit test. According to the exercise page, Grading of Exercises, point 4 -24p.
package ssw.mj.symtab;

import ssw.mj.Errors.Message;
import ssw.mj.Parser;
import ssw.mj.symtab.Obj.Kind;

/**
 * MicroJava Symbol Table
 */
public class Tab {
	// Universe
	
	public static final Struct noType = new Struct(Struct.Kind.None);
	public static final Struct intType = new Struct(Struct.Kind.Int);
	public static final Struct charType = new Struct(Struct.Kind.Char);
	public static final Struct nullType = new Struct(Struct.Kind.Class);

	public Obj noObj, chrObj, ordObj, lenObj;

	private final int LOCALITY=2;
	/** Only used for reporting errors. */
	private final Parser parser;
	/** The current top scope. */
	public Scope curScope;
	/** Nesting level of current scope. */
	private int curLevel;


	// TODO Exercise 4: implementation of symbol table
	public void openScope(){
		curScope=new Scope(curScope);
		curLevel++;
	}
	
	public void closeScope(){
		curScope=curScope.outer();
		curLevel--;
	}
	
	public Obj insert(Obj.Kind kind, String name, Struct type){
		Obj o = new Obj(kind, name, type);

		//Checking that the name is not already used
		Obj found = curScope.findLocal(name);
		if(found!=null && found.kind!=Obj.Kind.Prog){
			parser.error(Message.DECL_NAME,name);
		}
		if(curLevel<LOCALITY){
			found = curScope.findGlobal(name);
			if(found!=null && found.kind!=Obj.Kind.Prog){
				parser.error(Message.DECL_NAME,name);
			}
		}
		//Checking if the variable is local or global
		if(kind==Obj.Kind.Var){
			if(curLevel>=2)
			o.level=1;else o.level=0;
			o.adr=curScope.nVars();
		}
		
		curScope.insert(o);
		return o;
	}
	
	public Obj find(String name, boolean silent){
		Obj res = curScope.findLocal(name);
		if(res!=null){
			return res;
		}
		res = curScope.findGlobal(name);
		if(res==null){
			//if(name.equals("print")){
			//	res=new Obj(Obj.Kind.Meth,"print",new Struct(Struct.Kind.None));
			//}else{
			if(!silent) parser.error(Message.NOT_FOUND,name);
			res=new Obj(Obj.Kind.Con,"",new Struct(Struct.Kind.None));
			//}
		}
		return res;
	}
	
	public Obj findField(String name, Struct type){
		Obj field = type.findField(name);
		if(field==null){
			parser.error(Message.NO_FIELD,name);
			return new Obj(Obj.Kind.Con,"",new Struct(Struct.Kind.None));
		}
			
		return type.findField(name);
	}
	
	/**
	 * Set up "universe" (= predefined names).
	 */
	public Tab(Parser p) {
		
		parser = p;
		curScope=new Scope();
		
		insert(Kind.Type,"int",intType);
		insert(Kind.Type,"char",charType);
		insert(Kind.Con,"null",nullType);
		chrObj=insert(Kind.Meth,"chr",charType);
		chrObj.nPars=1;
		chrObj.level=1;
		Obj var = new Obj(Kind.Var,"i",intType);
		var.level=1;
		var.adr=0;
		chrObj.locals.put("i", var);
		
		ordObj=insert(Kind.Meth,"ord",intType);
		ordObj.nPars=1;
		ordObj.level=1;
		var = new Obj(Kind.Var,"ch",charType);
		var.level=1;
		var.adr=0;
			ordObj.locals.put("ch", var);

		lenObj=insert(Kind.Meth,"len",intType);
		lenObj.nPars=1;
		lenObj.level=1;
		var = new Obj(Kind.Var,"arr",new Struct(noType));
		var.level=1;
		var.adr=0;
			lenObj.locals.put("arr",var);
		
		
		//openScope();
		noObj=new Obj(Kind.Var, "noObj",noType);
	}
}
