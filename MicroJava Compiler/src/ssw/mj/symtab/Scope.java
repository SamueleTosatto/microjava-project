package ssw.mj.symtab;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * MicroJava Symbol Table Scopes
 */
public class Scope {
	/** Reference to enclosing scope. */
	private Scope outer;
	/** Declarations of this scope. */
	private Map<String, Obj> locals = new LinkedHashMap<String, Obj>();
	/** Number of variables in this scope. */
	private int nVars;

	public Scope(Scope outer) {
		this.outer = outer;
	}
	

	public Scope() {
		// TODO Auto-generated constructor stub
	}


	public int nVars() {
		return nVars;
	}

	Obj findGlobal(String name) {
		Obj res = findLocal(name);
		if (res == null && outer != null) {
			res = outer.findGlobal(name);
		}
		return res;
	}

	Obj findLocal(String name) {
		return locals.get(name);
	}

	void insert(Obj o) {
		locals.put(o.name, o);
		if (o.kind == Obj.Kind.Var) {
			nVars++;
		}
	}

	public Scope outer() {
		return outer;
	}

	public Map<String, Obj> locals() {
		return Collections.unmodifiableMap(locals);
	}
}
