package ssw.mj;

import ssw.mj.Errors.Message;

public class ParserSupport{

	/** Maximum number of global variables per program */
	protected static final int MAX_GLOBALS = 32767;

	/** Maximum number of fields per class */
	protected static final int MAX_FIELDS = 32767;

	/** Maximum number of local variables per method */
	protected static final int MAX_LOCALS = 127;

	protected static final String MAIN_="main";
	/** Last recognized token; */
	protected Token t;
	/** Lookahead token (not recognized).) */
	protected Token la;
	protected static final int N_ERRORS=2;
	protected int nRightScan=N_ERRORS;

	/** Shortcut to kind attribute of lookahead token (la). */

	protected Token.Kind sym;

	/** According scanner */
	public final Scanner scanner;


	/**
	 * Initialization of parser. Need a scanner as parameter
	 * @param scanner
	 */
	public ParserSupport(Scanner scanner) {
		this.scanner = scanner;
		la = new Token(Token.Kind.none, 1, 1);
	}
	/**
	 * Adds error message to the list of errors.
	 */
	public void error(Message msg, Object... msgParams) {
		if(nRightScan>N_ERRORS)
			scanner.errors.error(la.line, la.col, msg, msgParams);
		nRightScan=0;
	}
	/**
	 * This method retrive the next token from the scanner
	 */
	protected void scan () {
		 t = la; 
		 la = scanner.next(); 
		 sym = la.kind;
		 nRightScan++;
		 nRightScan=(nRightScan>4)?4:nRightScan;
	}
	/**
	 * This check the token we expect is the same as the actual one.
	 * If is true then we just scan the new token, we throw an error otherwise. 
	 * @param expected
	 */
	protected boolean check (Token.Kind expected) {
		 if (sym == expected) {
			 scan();
			 return true;
		 } else {
			 error(Message.TOKEN_EXPECTED, expected);
			 return false;
		 }
	}
}
