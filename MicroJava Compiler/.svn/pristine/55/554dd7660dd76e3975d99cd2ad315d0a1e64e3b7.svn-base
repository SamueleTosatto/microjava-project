package ssw.mj;

import static ssw.mj.Token.Kind.none;
import ssw.mj.Errors.Message;
import ssw.mj.codegen.Code;
import ssw.mj.symtab.Tab;

/**
 * The Parser for the MicroJava Compiler.
 */
public class Parser {

	/** Maximum number of global variables per program */
	@SuppressWarnings("unused")
	private static final int MAX_GLOBALS = 32767;

	/** Maximum number of fields per class */
	@SuppressWarnings("unused")
	private static final int MAX_FIELDS = 32767;

	/** Maximum number of local variables per method */
	@SuppressWarnings("unused")
	private static final int MAX_LOCALS = 127;

	/** Last recognized token; */
	@SuppressWarnings("unused")
	private Token t;

	/** Lookahead token (not recognized).) */
	private Token la;

	/** Shortcut to kind attribute of lookahead token (la). */
	@SuppressWarnings("unused")
	private Token.Kind sym;

	/** According scanner */
	public final Scanner scanner;

	/** According code buffer */
	public final Code code;

	/** According symbol table */
	public final Tab tab;

	// TODO Exercise 3 - 6: implementation of parser

	public Parser(Scanner scanner) {
		this.scanner = scanner;
		tab = new Tab(this);
		// Avoid crash when 1st symbol has scanner error.
		la = new Token(none, 1, 1);
		code = new Code(this);
	}

	/**
	 * Adds error message to the list of errors.
	 */
	public void error(Message msg, Object... msgParams) {
		scanner.errors.error(la.line, la.col, msg, msgParams);
		// panic mode
		throw new Errors.PanicMode();
	}

	/**
	 * Starts the analysis.
	 */
	public void parse() {
	}
}
