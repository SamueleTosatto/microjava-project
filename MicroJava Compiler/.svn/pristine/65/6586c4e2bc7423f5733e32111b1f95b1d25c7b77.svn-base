package ssw.mj.codegen;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;

import ssw.mj.Errors.Message;
import ssw.mj.Parser;
import ssw.mj.symtab.Tab;

/**
 * MicroJava Code Generator.
 */
public class Code {
	public static enum OpCode {
		load, load_0, load_1, load_2, load_3, store, store_0, store_1, store_2,
		store_3, getstatic, putstatic, getfield, putfield, const_0, const_1,
		const_2, const_3, const_4, const_5, const_m1, const_, add, sub, mul,
		div, rem, neg, shl, shr, inc, new_, newarray, aload, astore, baload,
		bastore, arraylength, pop, dup, dup2, jmp, jeq, jne, jlt, jle, jgt,
		jge, call, return_, enter, exit, read, print, bread, bprint, trap, nop,
		pow, root, swap;

		public int code() {
			return ordinal() + 1;
		}

		public String cleanName() {
			String name = name();
			if (name.endsWith("_")) {
				name = name.substring(0, name.length() - 1);
			}
			return name;
		}

		public static OpCode get(int code) {
			if (code < 1 || code > values().length) {
				return null;
			}
			return values()[code - 1];
		}
	}

	public static enum CompOp {
		eq, ne, lt, le, gt, ge;
		public static CompOp invert(CompOp op) {
			switch (op) {
			case eq:
				return ne;
			case ne:
				return eq;
			case lt:
				return ge;
			case le:
				return gt;
			case gt:
				return le;
			case ge:
				return lt;
			}
			throw new IllegalArgumentException("Unexpected compare operator");
		}
	}

	/** Code buffer */
	public byte[] buf;

	/**
	 * Program counter. Indicates next free byte in code buffer.
	 */
	public int pc;

	/** PC of main method (set by parser). */
	public int mainpc;

	/** Length of static data in words (set by parser). */
	public int dataSize;

	/** According parser. */
	@SuppressWarnings("unused")
	private Parser parser;

	// ----- initialization

	public Code(Parser p) {
		parser = p;
		buf = new byte[100];
		pc = 0;
		mainpc = -1;
		dataSize = 0;
	}

	// ----- code storage management

	public void put(OpCode code) {
		put(code.code());
	}

	public void put(int x) {
		if (pc == buf.length) {
			buf = Arrays.copyOf(buf, buf.length * 2);
		}
		buf[pc++] = (byte) x;
	}

	public void put2(int x) {
		put(x >> 8);
		put(x);
	}

	public void put4(int x) {
		put2(x >> 16);
		put2(x);
	}

	public void put2(int pos, int x) {
		int oldpc = pc;
		pc = pos;
		put2(x);
		pc = oldpc;
	}

	public int get(int pos) {
		return buf[pos];
	}

	public int get2(int pos) {
		return (get(pos) << 8) + (get(pos + 1) & 0xFF);
	}

	// TODO Exercise 5 - 6: implementation of code generation

	/**
	 * Write the code buffer to the output stream.
	 */
	public void write(OutputStream os) throws IOException {
		int codeSize = pc;
		// uncomment for debugging output
		// Decoder.decode(buf, 0, codeSize);

		ByteArrayOutputStream header = new ByteArrayOutputStream();
		DataOutputStream headerWriter = new DataOutputStream(header);
		headerWriter.writeByte('M');
		headerWriter.writeByte('J');
		headerWriter.writeInt(codeSize);
		headerWriter.writeInt(dataSize);
		headerWriter.writeInt(mainpc);
		headerWriter.close();

		os.write(header.toByteArray());

		os.write(buf, 0, codeSize);
		os.flush();
		os.close();
	}

	void loadConst(int val){
		switch (val) {
			case 0: put(OpCode.const_0); break;
			case 1: put(OpCode.const_1); break;
			case 2: put(OpCode.const_2); break;
			case 3: put(OpCode.const_3); break;
			case 4: put(OpCode.const_4); break;
			case 5: put(OpCode.const_5); break;
			case -1: put(OpCode.const_m1); break;
			default: put(OpCode.const_); put(val); break;
		}
	}
		
	void load(Operand x) {
		switch (x.kind) {
			case Con: loadConst(x.val); break;
			case Local:
				switch (x.adr) {
					case 0: put(OpCode.load_0); break;
					case 1: put(OpCode.load_1); break;
					case 2: put(OpCode.load_2); break;
					case 3: put(OpCode.load_3); break;
					default: put(OpCode.load); put(x.adr); break;
				}
				break;
			case Static: put(OpCode.getstatic); put2(x.adr); break;
			case Stack: break; // nothing to do (already loaded)
			case Fld: put(OpCode.getfield); put2(x.adr); break;
			case Elem:
				if (x.type == Tab.charType) { put(OpCode.baload); }
				else { put(OpCode.aload); }
				break;
			default: parser.error(Message.NO_VAL);
		}
		x.kind = Operand.Kind.Stack;
	}
	/**
	 * String representation for JUnit test cases.
	 */
	public String dump() {
		StringBuilder sb = new StringBuilder();
		Decoder dec = new Decoder();
		sb.append(dec.decode(buf, 0, pc));
		sb.append("\n#CodeSize: ");
		sb.append(pc);
		sb.append("\n#DataSize: ");
		sb.append(dataSize);
		sb.append("\n#MainPC: ");
		sb.append(mainpc);
		return sb.toString();
	}
}
