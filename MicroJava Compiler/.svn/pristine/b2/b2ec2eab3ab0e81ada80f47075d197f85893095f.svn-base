package ssw.mj;


import java.util.EnumSet;

import ssw.mj.Errors.Message;
import ssw.mj.Token.Kind;
import ssw.mj.codegen.Code;
import ssw.mj.codegen.Code.CompOp;
import ssw.mj.codegen.Code.OpCode;
import ssw.mj.codegen.Operand;
import ssw.mj.symtab.Obj;
import ssw.mj.symtab.Struct;
import ssw.mj.symtab.Tab;




/**
 * The Parser for the MicroJava Compiler.
 */
public class Parser extends ParserSupport {

	final private EnumSet<Kind> statementFirst = EnumSet.of(Kind.if_,Kind.while_,Kind.break_,Kind.read,Kind.return_,Kind.print,Kind.semicolon, Kind.ident,Kind.lbrace);
	final private EnumSet<Kind> conditionFirst = EnumSet.of(Kind.minus,Kind.ident,Kind.number,Kind.charConst,Kind.new_,Kind.lpar);
	final private EnumSet<Kind> exprFirst = EnumSet.of(Kind.minus,Kind.ident,Kind.number,Kind.charConst,Kind.new_,Kind.lpar);
	final private EnumSet<Kind> mulOpFirst = EnumSet.of(Kind.times,Kind.slash,Kind.rem,Kind.ttimes,Kind.bfslash);
	final private EnumSet<Kind> assignOpFirst = EnumSet.of(Kind.assign,Kind.timesas,Kind.plusas,Kind.minusas,Kind.remas,Kind.slashas);


	// TODO Exercise 3 - 6: implementation of parser

	public final Code code;
	
	public final Tab tab;
	
	public Parser(Scanner scanner) {
		super(scanner);
		tab = new Tab(this);
		code = new Code(this);
	}

	
	private void Program () {
	
		check(Token.Kind.program);
		check(Token.Kind.ident);
		
		Obj program = tab.insert(Obj.Kind.Prog, t.str, Tab.noType);
		
		tab.openScope();
		
		Decl();
		
		if(tab.curScope.nVars()>MAX_GLOBALS)
			error(Message.TOO_MANY_GLOBALS);
		code.dataSize=tab.curScope.nVars();
		check(Kind.lbrace);
		
		MethodDeclCycle();
		
		if(code.mainpc<0)
			error(Message.METH_NOT_FOUND,"main");
		
		check(Kind.rbrace);
		program.locals=tab.curScope.locals();
		tab.closeScope();
	}
	
	public void MethodDeclCycle(){
		while(true){
			if(!(sym==Kind.rbrace || sym==Kind.eof)){
				if(!(sym==Kind.ident || sym==Kind.void_))
					RecoverMeth();
				else
					MethodDecl();
			}else{
				break;
			}
		}
	}
	/**
	 * Declaration of a global
	 */
	private void Decl(){
		while(true){
			if(sym==Kind.final_){
				ConstDecl();
			}else if(sym==Kind.ident){
				VarDecl(true);
			}else if(sym==Kind.class_){
				ClassDecl();
			}else if(sym==Kind.lbrace || sym==Kind.eof){
				break;
			}else{
				RecoverDecl();
			}
		}
	}
/**
 * This methods recover the error from a method bad written
 */
	private void RecoverMeth(){
		error(Message.METH_DECL);
		do {
			scan();
		} while (!(sym==Kind.ident || sym==Kind.void_ || sym==Kind.eof));
	}
	/**
	 * 
	 */
	private void RecoverDecl(){
		error(Message.INVALID_DECL);
		do {
			scan();
		} while (!(sym==Kind.lbrace ||sym==Kind.final_ || sym==Kind.ident  || sym==Kind.class_|| sym==Kind.eof));

	}
	
	/**MethodDecl = ( Type | "void" ) ident "(" [ FormPars ] ")"
{ VarDecl } Block.*/
	private void MethodDecl() {
		
		Struct type=null;
		if(sym==Kind.ident){
			type = Type();
		}else if(sym == Kind.void_){
			scan();
			type=Tab.noType;
		}else{
			RecoverMeth();
			return;
		}
		
		check(Kind.ident);
		Obj method = tab.insert(Obj.Kind.Meth, t.str, type);
		
		tab.openScope();
		
		check(Kind.lpar);
		
		if(sym==Kind.ident){
			FormPars();
		}
		
	
		check(Kind.rpar);
		
		//special checking for the main method (we assume that every method called main is a main method)
		if(method.name.equals(MAIN_) && method.type.kind!=Struct.Kind.None)
			error(Message.MAIN_NOT_VOID);
		if(method.name.equals(MAIN_) && tab.curScope.nVars()>0)
			error(Message.MAIN_WITH_PARAMS);
		if(method.name.equals(MAIN_)){
			System.out.println("[main found at:"+method.adr+"]");
			code.mainpc = code.pc;
		}
		method.nPars=tab.curScope.nVars();
		
		
		while(sym==Kind.ident){
			VarDecl(false);
		}
		
		if(tab.curScope.nVars()>MAX_LOCALS)
			error(Message.TOO_MANY_LOCALS);
		code.put(OpCode.enter);
		code.put(0);
		code.put(tab.curScope.nVars()-1);

		Block();

		method.locals=tab.curScope.locals();
		tab.closeScope();
		
		code.put(OpCode.exit);
		code.put(OpCode.return_);
	}
	/**
	 * FormPars = Type ident { "," Type ident }.
	 */
	private void FormPars() {
		
		Struct type = Type();
		
		check(Kind.ident);
		tab.insert(Obj.Kind.Var, t.str, type);
		
		while(sym==Kind.comma){
			scan();
			type = Type();
			check(Kind.ident);
			tab.insert(Obj.Kind.Var, t.str, type);
		}
		
	}

	private void Block() {
		
		check(Kind.lbrace);

			StatementCycle();
		
		check(Kind.rbrace);
	}
	
	private void StatementCycle(){
		while(true) {
			  if (statementFirst.contains(sym)) {
			    Statement();
			  } else if ( sym==Kind.rbrace  || sym == Kind.eof ){
				  break;
			  } else {
			    RecoverStat();
			  }
			}
	}
	
	private void isIntOperation(Operand x){
		if(x.type.kind!=Struct.Kind.Int)
			error(Message.NO_INT);
	}
	private void isMethod(Operand x){
		if(x.kind!=Operand.Kind.Meth)
			error(Message.NO_METH);
	}
	private void Statement_Designator(){
		Operand x = Designator();
		Operand.Kind k = x.kind;
		
		if(assignOpFirst.contains(sym)){
			Kind as = AssignOp();
			if(as==Kind.assign){
				Operand y = Expr();
				code.load(y);
				if(y.type.assignableTo(x.type))
					code.assign(x,y);
				else
					error(Message.INCOMP_TYPES);
			}else {
				code.selfAssignLoading(x);
				
				Operand y = Expr();
				isIntOperationOp(x);
				isIntOperationOp(y);
				//code.put(OpCode.dup);
				code.load(y);
				
				if(x.type.kind!=Struct.Kind.Int || y.type.kind!=Struct.Kind.Int)
					error(Message.NO_INT);
				if(as==Kind.minusas)
					code.put(OpCode.sub);
				else if(as==Kind.timesas)
					code.put(OpCode.mul);
				else if(as==Kind.slashas)
					code.put(OpCode.div);
				else if(as==Kind.remas)
					code.put(OpCode.rem);
				else if(as==Kind.plusas)
					code.put(OpCode.add);
				else
					code.put(OpCode.nop);
				code.assign(x);
				
			}
			
		}else if(sym==Kind.lpar){
			isMethod(x);
			ActPars();
		}else if(sym==Kind.pplus){

			if(x.kind!=Operand.Kind.Elem && x.kind!=Operand.Kind.Fld &&  x.kind!=Operand.Kind.Local &&  x.kind!=Operand.Kind.Static  )
				error(Message.NO_VAR);
			isIntOperation(x);
			scan();
			if(x.kind==Operand.Kind.Local){
				code.put(OpCode.inc);
				code.put(x.adr);
				code.put(1);
			}else{
				
				code.selfAssignLoading(x);
				code.load(new Operand(1));
				code.put(OpCode.add);
				x.kind=k;
				code.assign(x);
			}
			
		}else if(sym==Kind.mminus){
			checkVal(x);
			
			isIntOperation(x);
			scan();
			if(x.kind==Operand.Kind.Local){
				code.put(OpCode.inc);
				code.put(x.adr);
				code.put(-1);
			}else{
				//code.put(OpCode.dup);
				code.selfAssignLoading(x);
				code.load(new Operand(-1));
				code.put(OpCode.add);
				x.kind=k;
				code.assign(x);
			}
		}else{
			error(Message.DESIGN_FOLLOW);
		}
		check(Kind.semicolon);
	}
	private void isIntOperationOp(Operand x) {
		if(x.type.kind!=Struct.Kind.Int)
			error(Message.NO_INT_OP);
	}


	/**
	 * ActPars = "(" [ Expr { "," Expr } ] ")"
	 */
	private void ActPars() {
		check(Kind.lpar);
		
		if(exprFirst.contains(sym)){
			
			Expr();
			
			while(sym==Kind.comma){
				scan();
				Expr();
			}
		}
		
		check(Kind.rpar);
		
	}
	/**
	 * Expr = [ "–" ] Term { Addop Term }.
	 */
	private Operand Expr() {
		Operand x;
		if(sym==Kind.minus){
			scan();
			x = Term();
			if(x.type!=Tab.intType)
				error(Message.NO_INT_OP);
			if(x.kind==Operand.Kind.Con){
				x.val=-x.val;
				code.load(x);
			}else{
				code.load(x);
				code.put(OpCode.neg);
			}
			
		}else{
			x = Term();
			code.load(x);
		}
			
		while(sym==Kind.plus || sym==Kind.minus){
			OpCode opCode = AddOp();
			Operand y = Term();
			code.load(y);
			if(x.type!=Tab.intType || y.type!=Tab.intType)
				error(Message.NO_INT_OP);
			code.put(opCode);
		}
		return x;
		
	}
	/**
	 * Addop = "+" | "–".
	 */
	private OpCode AddOp() {
		if(sym==Kind.plus){
			scan();
			return OpCode.add;
		}else if(sym==Kind.minus){
			scan();
			return OpCode.sub;
		}else{
			error(Message.ADD_OP);
			return OpCode.nop;
		}
		
	}
	/**
	 * Term = Factor { Mulop Factor }.
	 */
	private Operand Term() {
		
		Operand x = Factor();
		
		while(mulOpFirst.contains(sym)){
			Code.OpCode opCode = MulOp();
			code.load(x);
			Operand y = Factor();
			code.load(y);
			if(x.type!=Tab.intType || y.type!=Tab.intType)
				error(Message.NO_INT_OP);
			if(opCode==Code.OpCode.root)
				code.put(Code.OpCode.swap);
			code.put(opCode);
		}
		
		return x;
		
	}
	/**
	 * Mulop = "*" | "/" | "%" | "**" | "\/".
	 */
	private Code.OpCode MulOp() {
		if(sym==Kind.times){
			scan();
			return Code.OpCode.mul;
		}else if(sym==Kind.slash){
			scan();
			return Code.OpCode.div;
		}else if(sym==Kind.rem){
			scan();
			return Code.OpCode.rem;
		}else if(sym==Kind.ttimes){
			scan();
			System.out.print("POW detected ");
			return Code.OpCode.pow;
		}else if(sym==Kind.bfslash){
			scan();
			return Code.OpCode.root;
		}else{
			error(Message.MUL_OP);
			return OpCode.nop;
		}
		
	}
	/**
	 * Factor = Designator [ ActPars ]
			| number
			| charConst
			| "new" ident [ "[" Expr "]" ]
			| "(" Expr ")".
	 */
	
	private Operand Factor() {
		Operand x;
		//Operand x;
		//String name;
		//Designator [ ActPars ]
		if(sym==Kind.ident){
			x = Designator();
			if(sym==Kind.lpar){
				if(x.type.kind==Struct.Kind.None)
					error(Message.INVALID_CALL);
				isMethod(x);
				ActPars();
			}else{
				checkVal(x);
			}
			//number
		}else if(sym==Kind.number){
			scan();
			x = new Operand(t.val);
			x.type=Tab.intType;
		}else if(sym==Kind.charConst){
			scan();
			x = new Operand(t.val);
			x.type=Tab.charType;
		}else if(sym==Kind.new_){
			
			scan();
			check(Kind.ident);
			Obj obj = tab.find(t.str,true);
			Struct type = obj.type;
			if(obj.kind!=Obj.Kind.Type)
				error(Message.NO_TYPE);
			if(sym==Kind.lbrack){
				scan();
				x = Expr();
				if(x.type!=Tab.intType)
					error(Message.ARRAY_SIZE);
				code.load(x);
				code.put(Code.OpCode.newarray);
				if(type==Tab.charType)
					code.put(0);
				else
					code.put(1);
				type = new Struct(Struct.Kind.Arr,type);
				
				check(Kind.rbrack);
			}else{
				if(obj.kind!=Obj.Kind.Type || type.kind!=Struct.Kind.Class)
					error(Message.NO_CLASS_TYPE);
				code.put(Code.OpCode.new_);
				code.put2(type.nrFields());
				
			}
			x =new Operand(type);
			x.kind=Operand.Kind.Stack;
			//"(" Expr ")".
		}else if(sym==Kind.lpar){
			
			scan();
			x=Expr();
			check(Kind.rpar);
		}else{
			error(Message.INVALID_FACT);
			//Just a "random" Operand, so that we can continue to generate code
			x=new Operand(0);
		}
		
		return x;
	}
	
	/**
	 * Assignop = "=" | "+=" | "-=" | "*=" | "/=" | "%=".
	 */
	private Kind AssignOp() {
		Kind ret = sym;
		
		//It is unuseless to check if sym belongs to the set {"==","+=",..} 
		//because this condition is checked by the caller
		//and we don't want to generate death or unusless code.
		
		scan();

		
		return ret;
	}
	/**
	 * Designator = ident { "." ident | "[" Expr "]" }.
	 */
	private Operand Designator() {
		
		check(Kind.ident);
		Obj ty;
		ty=tab.find(t.str,false);

		Operand x = new Operand(ty, this);
		while(true){
			
			if(sym==Kind.lbrack){
				
				checkVal(x);
				
				scan();
				
				code.load(x);
				
				Operand y = Expr();
				if(y.type.kind!=Struct.Kind.Int)
					error(Message.ARRAY_INDEX);
				code.load(y);
				if(x.type.kind!=Struct.Kind.Arr){
					error(Message.NO_ARRAY);
				}else{
					x.type = x.type.elemType;
				}
				x.kind = Operand.Kind.Elem; 
				check(Kind.rbrack);
				
				
			}else if(sym==Kind.period){
				
				checkVal(x);
				
				if(x.type.kind!=Struct.Kind.Class)
					error(Message.NO_CLASS);
				
				scan();
				code.load(x);
				check(Kind.ident);
				Obj obj = tab.findField(t.str, x.type);
				x.kind = Operand.Kind.Fld; x.type = obj.type; x.adr = obj.adr;
						
			}else{
				break;
			}
		}
		return x;
		
	}
	/**
	 * Check the operand it a value
	 * @param x
	 */
	private void checkVal(Operand x){
		if(x.kind==Operand.Kind.Stack || x.kind==Operand.Kind.Meth)
			error(Message.NO_VAL);
	}
	/**
	 * Statement = Designator ( Assignop Expr | ActPars | "++" | "--" ) ";"
					| "if" "(" Condition ")" Statement [ "else" Statement ]
					| "while" "(" Condition ")" Statement
					| "break" ";"
					| "return" [ Expr ] ";"
					| "read" "(" Designator ")" ";"
					| "print" "(" Expr [ "," number ] ")" ";"
					| Block
					| ";".
	 */
	private void Statement() {
		// Designator ( Assignop Expr | ActPars | "++" | "--" ) ";"
		if(sym==Kind.ident){
			Statement_Designator();
			//"if" "(" Condition ")" Statement [ "else" Statement ]
		}else if(sym==Kind.if_){
			Statement_If();
			//"while" "(" Condition ")" Statement
		}else if(sym==Kind.while_){
			Statement_While();
			//"break" ";"
		}else if(sym==Kind.break_){
			Statement_Break();
			//"return" [ Expr ] ";"
		}else if(sym==Kind.return_){
			Statement_Return();
			//"read" "(" Designator ")" ";"
		}else if(sym==Kind.read){
			Statement_Read();
			//"print" "(" Expr [ "," number ] ")" ";"
		}else if(sym==Kind.print){
			Statement_Print();
			//Block
		}else if(sym==Kind.lbrace){
			Block();
			//";"
		}else if(sym==Kind.semicolon){
			scan();
		}
		
	}
	
	/**
	 * This method recover the Statement, that is meaning that skip all the token until it syncronize with a Statement, or it found the end of a Block, or the end of the code
	 */
	private void RecoverStat(){
		error(Message.INVALID_STAT);
		do{
			scan();
		}while(!(statementFirst.contains(sym)||sym==Kind.rbrace || sym==Kind.else_|| sym==Kind.eof || sym==Kind.else_) || sym==Kind.ident || sym==Kind.lbrace );
		
	}
	
	private void Statement_Print() {
		scan();
		check(Kind.lpar);
		Operand y = Expr();
		//code.load(y);
		if(sym==Kind.comma){
			scan();
			check(Kind.number);
			code.load(new Operand(t.val));
		}else{
			code.load(new Operand(0));
		}
		if(y.type.kind==Struct.Kind.Char)
			code.put(OpCode.bprint);
		else if(y.type.kind==Struct.Kind.Int)
			code.put(OpCode.print);
		else
			error(Message.PRINT_VALUE);

		check(Kind.rpar);
		check(Kind.semicolon);
	}

	private void Statement_Read() {
		scan();
		check(Kind.lpar);
		Operand x = Designator();
		
		if(x.type.kind==Struct.Kind.Char)
			code.put(OpCode.bread);
		else if(x.type.kind==Struct.Kind.Int)
			code.put(OpCode.read);
		else
			error(Message.READ_VALUE);
		
		code.assign(x);
		check(Kind.rpar);
		check(Kind.semicolon);
	}

	private void Statement_Return() {
		scan();
		if(exprFirst.contains(sym)){
			Expr();
			check(Kind.semicolon);
		}else if(sym==Kind.semicolon){
			scan();
		}else{
			error(Message.INVALID_STAT);
		}
	}

	private void Statement_Break() {
		scan();
		check(Kind.semicolon);
	}

	private void Statement_While() {
		scan();
		check(Kind.lpar);
		if(conditionFirst.contains(sym)){
			Condition();
		}else{
			error(Message.INVALID_STAT);
		}
		check(Kind.rpar);
		Statement();
	}
	/**
	 * Condition = CondTerm { "||" CondTerm }.
	 */
	/**
	 * Condition = CondTerm { "||" CondTerm }.
	 */
	private Operand Condition() {
		Operand x = CondTerm();
		code.tJump(x);
		while(sym==Kind.or){
			scan();
			x=CondTerm();
			code.tJump(x);
		}
		return x;
	}
	/**
	 * CondTerm = CondFact { "&&" CondFact }.
	 */
	private Operand CondTerm() {
		Operand x = CondFact();
		code.fJump(x);
		while(sym==Kind.and){
			scan();
			x = CondFact();
			code.fJump(x);
		}
		return x;
	}
	/**
	 * CondFact = Expr Relop Expr
	 */
	private Operand CondFact() {
		Operand x = Expr();
		Operand y = Relop();
		Operand z = Expr();
		code.load(x);
		code.load(z);
		
		return y;
	}
	/**
	 * Relop = "==" | "!=" | ">" | ">=" | "<" | "<=".
	 */
	private Operand Relop() {
		Operand ret;
		if(sym==Kind.eql){
			ret =  new Operand(CompOp.eq,code);
		}else if(sym==Kind.neq){
			ret =  new Operand(CompOp.ne,code);
		}else if(sym==Kind.geq){
			ret =  new Operand(CompOp.ge,code);
		}else if(sym==Kind.gtr){
			ret =  new Operand(CompOp.gt,code);
		}else if(sym==Kind.lss){
			ret =  new Operand(CompOp.lt,code);
		}else if(sym==Kind.leq){
			ret = new Operand(CompOp.le,code);
		}else {
			ret =  new Operand(CompOp.eq,code);
			error(Message.REL_OP);
		}
		scan();
		return ret;
		
	}

	private void Statement_If() {
		scan();
		check(Kind.lpar);
		Condition();
		check(Kind.rpar);
		Statement();
		if(sym==Kind.else_){
			scan();
			Statement();
		}
	}
	/**
	 * ClassDecl = "class" ident "{" { VarDecl } "}".
	 */
	private void ClassDecl() {
		check(Kind.class_);
		check(Kind.ident);
		Obj myClass = tab.insert(Obj.Kind.Type, t.str, new Struct(Struct.Kind.Class));
		check(Kind.lbrace);
		tab.openScope();
		while(sym==Kind.ident){
			VarDecl(false);
		}
		if(tab.curScope.nVars()>MAX_FIELDS)
			error(Message.TOO_MANY_FIELDS);
		check(Kind.rbrace);
		
		myClass.type.fields = tab.curScope.locals();
		tab.closeScope();
	}

	
	private void VarDecl(boolean recover) {
		Struct type; //= Type();
		if(sym==Kind.ident){
			type = Type();
			if(type.kind==Struct.Kind.None && recover){
				RecoverDecl();
				return;
			}
			check(Kind.ident);
			tab.insert(Obj.Kind.Var, t.str, type);
			while(sym==Kind.comma){
				scan();
				check(Kind.ident);
				tab.insert(Obj.Kind.Var, t.str, type);
			}
			check(Kind.semicolon);
		}else{
			RecoverDecl();
		}
	}
	
	
	/**
	 * ConstDecl = "final" Type ident "=" ( number | charConst ) ";".
	 */
	private void ConstDecl(){
		check(Kind.final_);
		
		Struct type = Type();
		check(Kind.ident);
		Obj myObj = tab.insert(Obj.Kind.Con, t.str, type);
		
		check(Kind.assign);
		if(sym == Kind.number){// || sym == Kind.charConst){
			if(type.kind!=Struct.Kind.Int)
				error(Message.CONST_TYPE);
			scan();
			myObj.val=t.val;
		}else if(sym == Kind.charConst){// || sym == Kind.charConst){
			if(type.kind!=Struct.Kind.Char)
				error(Message.CONST_TYPE);
			scan();
			myObj.val=t.val;
		}else{
			error(Message.CONST_DECL);
		}
		check(Kind.semicolon);
	}
	/**
	 * Type = ident [ "[" "]" ].
	 */
	private Struct Type() {
		check(Kind.ident);
		Obj o = tab.find(t.str,false);
		

		if (o.kind != Obj.Kind.Type) {
			error(Message.NO_TYPE);
			
			return new Struct(Struct.Kind.None);
		}
		
		Struct type = o.type;
		if(sym==Kind.lbrack){
			scan();
			check(Kind.rbrack);
			type = new Struct(type);
		}
		return type;
	}

	/**
	 * Starts the analysis.
	 */
	public void parse() {
		scan();
		Program();
		check(Kind.eof);
		if(code.mainpc<0)
			error(Message.METH_NOT_FOUND,"main");
	}
}