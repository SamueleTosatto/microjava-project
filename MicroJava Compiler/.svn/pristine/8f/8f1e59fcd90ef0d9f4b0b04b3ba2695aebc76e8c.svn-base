package ssw.mj;

import java.io.IOException;
import java.io.Reader;

import ssw.mj.Errors.Message;
import ssw.mj.Token.Kind;

/**
 * The Scanner for the MicroJava Compiler.
 */
public class Scanner {
	@SuppressWarnings("unused")
	private static final char EOF = (char) -1;
	@SuppressWarnings("unused")
	private static final char LF = '\n';

	/** Input data to read from. */
	private Reader in;

	/** Lookahead character. (= next (unhandled) character in the input stream) */
	@SuppressWarnings("unused")
	private char ch = ' ';

	/** Current column in input stream. */
	private int col = 0;
	/** Current line in input stream. */
	private int line = 1;

	/** Are we inside a comment? */
	private boolean commentFlag = false;
	/** How many nested comment do we have?*/
	private int nestedComment = 0;
	/**did we find EOF?*/
	private boolean eof = false;
	
	
	/** According errors object. */
	public final Errors errors;

	// TODO Exercise 2: implementation of scanner

	public Scanner(Reader r) {
		// initialize error handling support
		errors = new Errors();
		in = r;
	}

	/**
	 * Adds error message to the list of errors.
	 */
	@SuppressWarnings("unused")
	private void error(Token t, Message msg, Object... msgParams) {
		errors.error(t.line, t.col, msg, msgParams);

		// reset token content (consistent JUnit tests)
		t.val = 0;
		t.str = null;
	}

	/**
	 * It reads the next char
	 */
	private void nextChar(){
		try{
			int x = in.read();
			if (x == -1) {
				this.eof = true;
				return;
			}
			ch = (char) x;
			if (ch == '\n') {
				//If a new line is found, reset col, and refresh line.
				col = 0;
				line++;
			} else {
				//If it is just another character, then increment col
				col++;
			}
		}catch(Exception ex){
			//TODO: Implement the exception
		}
	}

	/**
	 * It reads a Symbol or skip a comment
	 */
	public void readSymbol(Token t) {
		//We check what kind of symbol is read.
		//P.S. The author of the code prefer to use if..else instead switch
		if (ch == '/') {
			nextChar();
			//Here we are reading a comment... Let's launch the code for skip the comments
			if (ch == '*') {
				commentFlag = true;		//yes, we are reading a comment
				nestedComment = 1;		//and yes, there is one and only one open comment
				skipComment(t);			//and yes, of course we would like to skip this useless comment
				return;					//and make sure we are ending the method (just if I forgot some "else if" )
			} else if (ch == '=') {
				t.kind = Kind.slashas;
				nextChar();
			} else {
				t.kind = Kind.slash;
			}
		} else if (ch == '*') {
			nextChar();
			if (ch == '*') {
				t.kind = Kind.ttimes;
				nextChar();
			} else if (ch == '=') {
				t.kind = Kind.timesas;
				nextChar();
			} else {
				t.kind = Kind.times;
			}
		} else if (ch == '\\') {
			nextChar();
			if (ch == '/') {
				t.kind = Kind.bfslash;
				nextChar();
			} else {
				error(t, Message.INVALID_CHAR, '\\');
			}
		} else if (ch == '+') {
			nextChar();
			if (ch == '+') {
				t.kind = Kind.pplus;
				nextChar();
			} else if (ch == '=') {
				t.kind = Kind.plusas;
				nextChar();
			} else {
				t.kind = Kind.plus;
			}
		} else if (ch == '-') {
			nextChar();
			if (ch == '-') {
				t.kind = Kind.mminus;
				nextChar();
			} else if (ch == '=') {
				t.kind = Kind.minusas;
				nextChar();
			} else {
				t.kind = Kind.minus;
			}
		} else if (ch == '<') {
			nextChar();
			if (ch == '=') {
				t.kind = Kind.leq;
				nextChar();
			} else {
				t.kind = Kind.lss;
			}
		} else if (ch == '>') {
			nextChar();
			if (ch == '=') {
				t.kind = Kind.geq;
				nextChar();
			} else {
				t.kind = Kind.gtr;
			}
		} else if (ch == '%') {
			nextChar();
			if (ch == '=') {
				t.kind = Kind.remas;
				nextChar();
			} else {
				t.kind = Kind.rem;
			}
		} else if (ch == '&') {
			nextChar();
			if (ch == '&') {
				t.kind = Kind.and;
				nextChar();
			} else {
				error(t, Message.INVALID_CHAR, '&');
			}
		} else if (ch == '|') {
			nextChar();
			if (ch == '|') {
				t.kind = Kind.or;
				nextChar();
			} else {
				error(t, Message.INVALID_CHAR, '|');
			}
		} else if (ch == '!') {
			nextChar();
			if (ch == '=') {
				t.kind = Kind.neq;
				nextChar();
			} else {
				error(t, Message.INVALID_CHAR, '!');
			}
		} else if (ch == '=') {
			nextChar();
			if (ch == '=') {
				t.kind = Kind.eql;
				nextChar();
			} else {
				t.kind = Kind.assign;
			}
		} else if (ch == ',') {
			t.kind = Kind.comma;
			nextChar();
		} else if (ch == ';') {
			t.kind = Kind.semicolon;
			nextChar();
		} else if (ch == '.') {
			t.kind = Kind.period;
			nextChar();
		} else if (ch == '(') {
			t.kind = Kind.lpar;
			nextChar();
		} else if (ch == ')') {
			t.kind = Kind.rpar;
			nextChar();
		} else if (ch == '[') {
			t.kind = Kind.lbrack;
			nextChar();
		} else if (ch == ']') {
			t.kind = Kind.rbrack;
			nextChar();
		} else if (ch == '{') {
			t.kind = Kind.lbrace;
			nextChar();
		} else if (ch == '}') {
			t.kind = Kind.rbrace;
			nextChar();
		} else {
			//if ch didn't match to any of the previous, then it should be an invalid char.
			t.kind = Kind.none;
			error(t, Message.INVALID_CHAR, ch);
			nextChar();
		}
	}

	/**
	 * It reads the numbers
	 */
	private void readNumber(Token t)  {
		t.kind = Kind.number;
		//Let's read every char from the number
		do {
			t.str += ch;
			nextChar();
		} while (Character.isDigit(ch) && !eof); //until we don't reach something that is different from a digit
		try {
			t.val = Integer.parseInt(t.str);	//If it is possible to do the conversion it means that the number is not too big
		} catch (Exception ex) {
			error(t, Message.BIG_NUM, t.str);	//But if it is too big, let's raise an error
		}

	}
	/** Checks whenever a char could be part of the "body" of a java micro identifier**/
	private boolean isMicroJavaIdentifierPart(char ch){
		return ch>='a' && ch<='z' || ch>='A' && ch<='Z' || ch>='0' && ch<='9' || ch=='_';
	}
	/** Checks whenever a char could be first char of a java micro identifier**/
	private boolean isMicroJavaIdentifierStart(char ch){
		return ch>='a' && ch<='z' || ch>='A' && ch<='Z';
	}
	/**
	 * it deals with the identifiers, and the keyword
	 */
	private void readIdent(Token t){
		t.kind = Kind.ident;
		//Just let's read the Identifier
		while (isMicroJavaIdentifierPart(ch) && !eof) {
			t.str += ch;
			nextChar();
		}
		//Then let's check if it is a keyword
		if (t.str.equals("if")) {
			t.kind = Kind.if_;
		} else if (t.str.equals("while")) {
			t.kind = Kind.while_;
		} else if (t.str.equals("break")) {
			t.kind = Kind.break_;
		} else if (t.str.equals("final")) {
			t.kind = Kind.final_;
		} else if (t.str.equals("class")) {
			t.kind = Kind.class_;
		} else if (t.str.equals("program")) {
			t.kind = Kind.program;
		} else if (t.str.equals("print")) {
			t.kind = Kind.print;
		} else if (t.str.equals("return")) {
			t.kind = Kind.return_;
		} else if (t.str.equals("else")) {
			t.kind = Kind.else_;
		} else if (t.str.equals("read")) {
			t.kind = Kind.read;
		} else if (t.str.equals("new")) {
			t.kind = Kind.new_;
		} else if (t.str.equals("void")) {
			t.kind = Kind.void_;
		}
	}

	/**
	 * It skips the comments
	 */
	private void skipComment(Token t) {
		//Jump is a variable that checks when a char is already read and we need to control again the flow from that point and not from the next char
		boolean jump=false;
	
		while (true) {
			
			//Sometimes, the character we should read is already read. So in this case the boolean variable jump would be true.
			if(!jump)nextChar();
			else jump=false;
			
			//if EOF exit from the endless loop
			if (eof) {this.error(t, Message.EOF_IN_COMMENT);return;}
			
			//there is possibility of a comment ending
			if (ch == '*') {
				nextChar();
				
				//if EOF exit from the endless loop
				if (eof) {this.error(t, Message.EOF_IN_COMMENT);return;}
				
				//Okay we are in the case of a "CLOSE_COMMENT"
				if (ch == '/') {
					this.nestedComment--;
					//If it closes all the nested loop
					if (nestedComment == 0) {
						//read the next char
						nextChar();
						//go out
						return;
					}
				}else{	//If we are not in the case of */, then we read another character instead /, so we need to process also that character, that's why jump=true.
					jump=true;
				}
			} else if (ch == '/') {		//if there is a slash maybe we are opening a new comment
				nextChar();
				
				//If EOF we need to go out 
				if (eof) {this.error(t, Message.EOF_IN_COMMENT);return;}
				
				//If '*' if found after a '/' then it is a new nested comment.
				if (ch == '*') {
					this.nestedComment++;
				}else jump=true;	//If is not a new comment then for the same reason as before, we need to skip reading a new char
			}

		}
	}

	/**
	 * This skip all the while spaces, until it found a different char
	 * 
	 * @throws IOException
	 */
	private void skipWhite()  {
		while (Character.isWhitespace(ch) && !eof) {
			nextChar();
		}
	}

	/**
	 * It reads the char consts
	 * @param t
	 */
	private void readCharConst(Token t) {
		
		//Let's read the following character after the '
		t.kind = Kind.charConst;
		nextChar();
		
		//If the next is a EOF, then launch the error
		if(eof){error(t,Message.MISSING_QUOTE);return;}
		
		
		// case it is escape
		if (ch == '\\') {
			nextChar();
			
			if(eof){error(t,Message.MISSING_QUOTE);return;}
			
			//A valid Escape
			if (ch == 'r' || ch == 'n' || ch == '\'' || ch == '\\') {
				if (ch == 'r')
					t.val = '\r';
				if (ch == 'n')
					t.val = '\n';
				if (ch == '\'')
					t.val = '\'';
				if (ch == '\\')
					t.val = '\\';
				nextChar();
				quoteCheck(t);
			} 
			
			//An illegal new line
			else if(ch==LF || ch=='\n' || ch=='\r')	error(t, Message.ILLEGAL_LINE_END, ch);
			
			//or a char not valid for escaping
			else{
				error(t, Message.UNDEFINED_ESCAPE, ch);
				nextChar();
				quoteCheck(t);
			}
			
		} 
		
		//The empty char case
		else if (ch == '\'') {
			error(t, Message.EMPTY_CHARCONST);
			nextChar();
		} 
		//The line return case
		else if (ch==LF || ch=='\n' || ch=='\r') {
			error(t,Message.ILLEGAL_LINE_END);
			nextChar();
		}
		//Okay a right Character
		else{
			t.val = ch;
			nextChar();
			quoteCheck(t);
		}

	}
	/**
	 * Just check that a quote after a CharConst is placed
	 * @param t
	 */
	private void quoteCheck(Token t) {
		if (ch != '\'') 
			error(t, Message.MISSING_QUOTE);
		else
			nextChar();
	}
	/**
	 * The next token is given
	 * @return
	 * @throws IOException
	 */
	public Token next() {	
			//Hipotesis: we are not reading a comment
			commentFlag = false;
			//Skip all the white spaces
			skipWhite();
			//If the file is End..
			if (this.eof) {
				return new Token(Token.Kind.eof, line, col);
			}
			Token t = new Token(Token.Kind.none, line, col);
			t.str = "";

			//All the possible cases
			if (ch == '\'') {				//CharConstant
				readCharConst(t);
			} else if (isMicroJavaIdentifierStart(ch)) {						//MicroJavaIdentifier Starting
				readIdent(t);
			} else if (Character.isDigit(ch)) {									//A number
				readNumber(t);
			} else {
				this.readSymbol(t);												//Everything else could be a symbol (or an invalid character) or a comment
				if (commentFlag)
					return next();									//This recursion could be easly write as loop (with better performance probably), but the author believes that this is the more readable way
			}

			return t;
	}

}
