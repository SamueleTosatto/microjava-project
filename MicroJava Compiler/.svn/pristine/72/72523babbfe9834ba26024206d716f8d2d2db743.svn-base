//TODO CR 17/24
//TODO CR You didn't create a branch -3
package ssw.mj;

import static ssw.mj.Token.Kind.none;
import ssw.mj.Errors.Message;
import ssw.mj.codegen.Code;
import ssw.mj.symtab.Obj;
import ssw.mj.symtab.Obj.Kind;
import ssw.mj.symtab.Struct;
import ssw.mj.symtab.Tab;




/**
 * The Parser for the MicroJava Compiler.
 */
public class Parser {

	/** Maximum number of global variables per program */
	private static final int MAX_GLOBALS = 32767;

	/** Maximum number of fields per class */
	private static final int MAX_FIELDS = 32767;

	/** Maximum number of local variables per method */
	private static final int MAX_LOCALS = 127;

	private static final String MAIN_="main";
	/** Last recognized token; */
	private Token t;
	/** Lookahead token (not recognized).) */
	private Token la;
	private static final int N_ERRORS=2;
	private int nRightScan=N_ERRORS;

	/** Shortcut to kind attribute of lookahead token (la). */
	//TODO CR Remove unused annotation
	private Token.Kind sym;

	/** According scanner */
	public final Scanner scanner;

	/** According code buffer */
	public final Code code;

	/** According symbol table */
	public final Tab tab;
	
	public boolean error=false;

	// TODO Exercise 3 - 6: implementation of parser
	/**
	 * This class provide an abstraction on the property(ies maybe in the future) about a Non Terminal
	 * @author Samuele
	 *
	 */
	private static class NonTerminal{
		/**
		 * This method just check if a token t is inside a token list 
		 * @param s is a list (view better as a set) fo possible token
		 * @param t	is a token
		 * @return true if t is in s, false otherwise
		 */
		//TODO CR This is a very inefficient way to check for firsts. Use a Map for fast look-up instead of iterating over a list -1
		protected  static boolean isIn(Token.Kind[] s, Token.Kind t){
			for(Token.Kind x : s){
				if(x==t)
					return true;
			}
			return false;
		}
	}

	private static class ConstDecl extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.final_};
		public static  boolean hasFirst( Token.Kind k) {
			return isIn(first,k);
		}
	}
	
	private static class VarDecl extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.ident/*from Type*/};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
	}
	private static class ClassDecl extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.class_};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
	}
	private static class MethodDecl extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.void_,Token.Kind.ident/*from Type*/};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
	}
	private static class Type extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.ident};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
	}
	private static class Block extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.lbrace};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
	}

	private static class Statement extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.ident/*Designator*/,Token.Kind.if_,Token.Kind.while_,Token.Kind.break_,Token.Kind.read,Token.Kind.return_,Token.Kind.print,Token.Kind.semicolon,Token.Kind.lbrace/*from Block*/};
		static Token.Kind[] follow = new Token.Kind[]{Token.Kind.else_,Token.Kind.rbrace,Token.Kind.if_,Token.Kind.while_,Token.Kind.break_,Token.Kind.read,Token.Kind.return_,Token.Kind.print,Token.Kind.semicolon/*from Block*/};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
		public static boolean hasFollow(Token.Kind k){
			return isIn(follow,k);
		}
	}
	private static class AssignOp extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.assign,Token.Kind.plusas,Token.Kind.minusas,Token.Kind.timesas,Token.Kind.slashas,Token.Kind.remas};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
	}
	private static class ActPars extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.lpar};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
	}
	private static class Condition extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.minus,Token.Kind.ident/*Designator*/, Token.Kind.number,Token.Kind.charConst,Token.Kind.new_,Token.Kind.lpar};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
	}
	private static class CondTerm extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.minus,Token.Kind.ident/*Designator*/, Token.Kind.number,Token.Kind.charConst,Token.Kind.new_,Token.Kind.lpar};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
	}
	private static class CondFact extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.minus,Token.Kind.ident/*Designator*/, Token.Kind.number,Token.Kind.charConst,Token.Kind.new_,Token.Kind.lpar};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
	}
	private static class Expr extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.minus,Token.Kind.ident/*Designator*/, Token.Kind.number,Token.Kind.charConst,Token.Kind.new_,Token.Kind.lpar};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
	}
	private static class Term extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.ident/*Designator*/, Token.Kind.number,Token.Kind.charConst,Token.Kind.new_,Token.Kind.lpar};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
	}
	private static class Factor extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.ident/*Designator*/, Token.Kind.number,Token.Kind.charConst,Token.Kind.new_,Token.Kind.lpar};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
		public static boolean hasFollow(Token.Kind k){
			return AddOp.hasFirst(k) || MulOp.hasFirst(k) || k.equals(Token.Kind.comma) || k.equals(Token.Kind.rpar);
		}
	}
	private static class Designator extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.ident};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
	}
	private static class AddOp extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.plus,Token.Kind.minus};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
	}
	private static class MulOp extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.times,Token.Kind.slash,Token.Kind.rem,Token.Kind.ttimes,Token.Kind.bfslash};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
	}
	private static class FromPars extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.ident};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
	}
	/**
	 * Initialization of parser. Need a scanner as parameter
	 * @param scanner
	 */
	public Parser(Scanner scanner) {
		this.scanner = scanner;
		tab = new Tab(this);
		
		// Avoid crash when 1st symbol has scanner error.
		la = new Token(none, 1, 1);
		code = new Code(this);
	}

	/**
	 * Adds error message to the list of errors.
	 */
	public void error(Message msg, Object... msgParams) {
		if(nRightScan>N_ERRORS)
			scanner.errors.error(la.line, la.col, msg, msgParams);
		nRightScan=0;
		error=true;
		// panic mode
		//throw new Errors.PanicMode();
	}
	/**
	 * This method retrive the next token from the scanner
	 */
	private void scan () {
		 t = la; 
		 la = scanner.next(); 
		 sym = la.kind;
		 nRightScan++;
		 nRightScan=(nRightScan>4)?4:nRightScan;
	}
	/**
	 * This check the token we expect is the same as the actual one.
	 * If is true then we just scan the new token, we throw an error otherwise. 
	 * @param expected
	 */
	private void check (Token.Kind expected) {
		 if (sym == expected) {
			 scan();
		 } else {
			 error(Message.TOKEN_EXPECTED, expected);
		 }
	}
	/**
	 *Program =  "program" ident { ConstDecl | VarDecl | ClassDecl }
"{" {MethodDecl} "}"
	 */
	private void Program () {
		/*"program"*/
		check(Token.Kind.program);
		/*ident*/
		check(Token.Kind.ident);
		Obj program = tab.insert(Obj.Kind.Prog,
				 t.str, tab.noType);
		/*{ ConstDecl | VarDecl | ClassDecl }*/
		tab.openScope();
		while(true){//ConstDecl.hasFirst(sym) || VarDecl.hasFirst(sym) || ClassDecl.hasFirst(sym)){
			if(sym==Token.Kind.lbrace)
				break;
			Decl();
		}
		if(tab.curScope.nVars()>MAX_GLOBALS)
			error(Message.TOO_MANY_GLOBALS);
		/*"{"*/
		check(Token.Kind.lbrace);
		/*{ MethodDecl }*/
		while(true){
			MethodDecl();
			if(sym==Token.Kind.rbrace)
				break;
		}
		
		check(Token.Kind.rbrace);
		program.locals=tab.curScope.locals();
		tab.closeScope();
	}
	private void Decl(){
		if(ConstDecl.hasFirst(sym)){
			ConstDecl();
		}else if(VarDecl.hasFirst(sym)){
			VarDecl();
		}else if(ClassDecl.hasFirst(sym)){
			ClassDecl();
		}else{
			RecoverDecl();
		}
	}

	private void RecoverMeth(){
		error(Message.METH_DECL);
		do {
			scan();
		} while (sym != Token.Kind.rbrace && !MethodDecl.hasFirst(sym));
	}
	private void RecoverDecl(){
		error(Message.INVALID_DECL);
		do {
			scan();
		} while (sym != Token.Kind.lbrace && !ConstDecl.hasFirst(sym) && !VarDecl.hasFirst(sym) && !ClassDecl.hasFirst(sym) );

	}
	
	/**MethodDecl = ( Type | "void" ) ident "(" [ FormPars ] ")"
{ VarDecl } Block.*/
	private void MethodDecl() {
		//( Type | "void" )
		Struct type=null;
		if(Type.hasFirst(sym)){
			type = Type();
		}else if(sym == Token.Kind.void_){
			scan();
			type=Tab.noType;
		}else{
			RecoverMeth();
		}
		//ident
		check(Token.Kind.ident);
		Obj method = tab.insert(Obj.Kind.Meth, t.str, type);
		
		tab.openScope();
		
		check(Token.Kind.lpar);
		//[ FormPars ] 
		if(FromPars.hasFirst(sym)){
			FormPars();
		}
		
		
		//TODO it is imprecise, because i guess that if main is just a method of another class, and not a name in the Progr, tham it could have more than zero param
		
		//")"
		check(Token.Kind.rpar);
		
		//special checking for the main method (we assume that every method called main is a main method)
		if(method.name.equals(MAIN_) && method.type.kind!=Struct.Kind.None)
			error(Message.MAIN_NOT_VOID);
		if(method.name.equals(MAIN_) && tab.curScope.nVars()>0)
			error(Message.MAIN_WITH_PARAMS);
		method.nPars=tab.curScope.nVars();
		
		//{ VarDecl }
		while(VarDecl.hasFirst(sym)){
			VarDecl();
		}
		if(tab.curScope.nVars()>MAX_LOCALS)
			error(Message.TOO_MANY_LOCALS);
		//Block
		Block();
		method.locals=tab.curScope.locals();
		tab.closeScope();
	}
	/**
	 * FormPars = Type ident { "," Type ident }.
	 */
	private void FormPars() {
		//Type
		Struct type = Type();
		//ident
		check(Token.Kind.ident);
		tab.insert(Obj.Kind.Var, t.str, type);
		//{"," Type ident
		while(sym==Token.Kind.comma){
			scan();
			type = Type();
			check(Token.Kind.ident);
			tab.insert(Obj.Kind.Var, t.str, type);
		}
		
	}
	/**
	 * Block = "{" { Statement } "}".
	 * 
	 */
	private void Block() {
		//"("
		check(Token.Kind.lbrace);
		//{Statement}
		while(true){
			//if(Statement.hasFirst(sym))
			if(sym==Token.Kind.rbrace)
				break;
			Statement();
		}
		//")"
		check(Token.Kind.rbrace);
	}

	private void Statement_Designator(){
		Designator();
		if(AssignOp.hasFirst(sym)){
			AssignOp();
			Expr();
		}else if(ActPars.hasFirst(sym)){
			ActPars();
		}else if(sym==Token.Kind.pplus){
			scan();
		}else if(sym==Token.Kind.mminus){
			scan();
		}else{
			error(Message.DESIGN_FOLLOW);
		}
		check(Token.Kind.semicolon);
	}
	/**
	 * ActPars = "(" [ Expr { "," Expr } ] ")"
	 */
	private void ActPars() {
		//"("
		check(Token.Kind.lpar);
		//[
		if(Expr.hasFirst(sym)){
			//Expr
			Expr();
			//{"," Expr}
			while(sym==Token.Kind.comma){
				scan();
				Expr();
			}
		}//]
		//")"
		check(Token.Kind.rpar);
		
	}
	/**
	 * Expr = [ "–" ] Term { Addop Term }.
	 */
	private void Expr() {
		if(sym==Token.Kind.minus){
			scan();
		}
		Term();
		while(AddOp.hasFirst(sym)){
			AddOp();
			Term();
		}
		
	}
	/**
	 * Addop = "+" | "–".
	 */
	private void AddOp() {
		if(sym==Token.Kind.plus){
			scan();
		}else if(sym==Token.Kind.minus){
			scan();
		}else{
			error(Message.ADD_OP);
		}
		
	}
	/**
	 * Term = Factor { Mulop Factor }.
	 */
	private void Term() {
		//factor
		Factor();
		//{Mulop Factor}
		while(MulOp.hasFirst(sym)){
			MulOp();
			Factor();
		}
		
	}
	/**
	 * Mulop = "*" | "/" | "%" | "**" | "\/".
	 */
	private void MulOp() {
		if(sym==Token.Kind.times){
			scan();
		}else if(sym==Token.Kind.slash){
			scan();
		}else if(sym==Token.Kind.rem){
			scan();
		}else if(sym==Token.Kind.ttimes){
			scan();
		}else if(sym==Token.Kind.bfslash){
			scan();
		}else{
			error(Message.MUL_OP);
		}
		
	}
	/**
	 * Factor = Designator [ ActPars ]
			| number
			| charConst
			| "new" ident [ "[" Expr "]" ]
			| "(" Expr ")".
	 */
	private void Factor() {
		//Designator [ ActPars ]
		if(Designator.hasFirst(sym)){//posso rimuovere
			Designator();
			if(ActPars.hasFirst(sym)){
				ActPars();
			}
			//number
		}else if(sym==Token.Kind.number){
			scan();
			//charConst
		}else if(sym==Token.Kind.charConst){
			scan();
			//new ident [ "[" Expr "]" ]
		}else if(sym==Token.Kind.new_){
			scan();
			check(Token.Kind.ident);
			if(sym==Token.Kind.lbrack){
				scan();
				Expr();
				check(Token.Kind.rbrack);
			}//"(" Expr ")".
		}else if(sym==Token.Kind.lpar){
			scan();
			Expr();
			check(Token.Kind.rpar);
			//else... Error
		}else{
			error(Message.INVALID_FACT);
		}
	}
	
	/**
	 * Assignop = "=" | "+=" | "-=" | "*=" | "/=" | "%=".
	 */
	private void AssignOp() {
		//TODO CR You defined first sets (in a inefficient way, but they are there) - why don't you use it? -1
		if(sym==Token.Kind.timesas){
			scan();
		}else if(sym==Token.Kind.slashas){
			scan();
		}else if(sym==Token.Kind.remas){
			scan();
		}else if(sym==Token.Kind.assign){
			scan();
		}else if(sym==Token.Kind.minusas){
			scan();
		}else if(sym==Token.Kind.plusas){
			scan();
		}else{
			error(Message.ASSIGN_OP);
		}
	}
	/**
	 * Designator = ident { "." ident | "[" Expr "]" }.
	 */
	private void Designator() {
		//ident
		check(Token.Kind.ident);
		//{
		//TODO CR Again, you double-check. Here the best solution is a forever loop 
		while(sym==Token.Kind.period|| sym==Token.Kind.lbrack){
			//"[" expr "]"
			if(sym==Token.Kind.lbrack){
				scan();
				Expr();
				check(Token.Kind.rbrack);
				//"." ident
			}else if(sym==Token.Kind.period){
				scan();
				check(Token.Kind.ident);
			}
		}//}
		
	}
	/**
	 * Statement = Designator ( Assignop Expr | ActPars | "++" | "--" ) ";"
					| "if" "(" Condition ")" Statement [ "else" Statement ]
					| "while" "(" Condition ")" Statement
					| "break" ";"
					| "return" [ Expr ] ";"
					| "read" "(" Designator ")" ";"
					| "print" "(" Expr [ "," number ] ")" ";"
					| Block
					| ";".
	 */
	private void Statement() {
		// Designator ( Assignop Expr | ActPars | "++" | "--" ) ";"
		if(Designator.hasFirst(sym)){
			Statement_Designator();
			//"if" "(" Condition ")" Statement [ "else" Statement ]
		}else if(sym==Token.Kind.if_){
			Statement_If();
			//"while" "(" Condition ")" Statement
		}else if(sym==Token.Kind.while_){
			Statement_While();
			//"break" ";"
		}else if(sym==Token.Kind.break_){
			Statement_Break();
			//"return" [ Expr ] ";"
		}else if(sym==Token.Kind.return_){
			Statement_Return();
			//"read" "(" Designator ")" ";"
		}else if(sym==Token.Kind.read){
			Statement_Read();
			//"print" "(" Expr [ "," number ] ")" ";"
		}else if(sym==Token.Kind.print){
			Statement_Print();
			//Block
		}else if(Block.hasFirst(sym)){
			Block();
			//";"
		}else if(sym==Token.Kind.semicolon){
			scan();
			//else.. Error!
		}else{
			RecoverStat();
		}
		
	}
	
	private void RecoverStat(){
		error(Message.INVALID_STAT);
		do{
			scan();
		}while(!Statement.hasFollow(sym));
	}
	
	private void Statement_Print() {
		scan();
		check(Token.Kind.lpar);
		Expr();
		if(sym==Token.Kind.comma){
			scan();
			check(Token.Kind.number);
			check(Token.Kind.rpar);
			check(Token.Kind.semicolon);
		}else{
			check(Token.Kind.rpar);
			check(Token.Kind.semicolon);
		}
	}

	private void Statement_Read() {
		scan();
		check(Token.Kind.lpar);
		Designator();
		check(Token.Kind.rpar);
		check(Token.Kind.semicolon);
	}

	private void Statement_Return() {
		scan();
		if(Expr.hasFirst(sym)){
			Expr();
			check(Token.Kind.semicolon);
		}else if(sym==Token.Kind.semicolon){
			scan();
		}else{
			error(Message.INVALID_STAT);
		}
	}

	private void Statement_Break() {
		scan();
		check(Token.Kind.semicolon);
	}

	private void Statement_While() {
		scan();
		check(Token.Kind.lpar);
		if(Condition.hasFirst(sym)){
			Condition();
		}else{
			error(Message.INVALID_STAT);
		}
		check(Token.Kind.rpar);
		Statement();
	}
	/**
	 * Condition = CondTerm { "||" CondTerm }.
	 */
	private void Condition() {
		CondTerm();
		while(sym==Token.Kind.or){
			scan();
			CondTerm();
		}
	}
	/**
	 * CondTerm = CondFact { "&&" CondFact }.
	 */
	private void CondTerm() {
		CondFact();
		while(sym==Token.Kind.and){
			scan();
			CondFact();
		}
	}
	/**
	 * CondFact = Expr Relop Expr
	 */
	private void CondFact() {
		Expr();
		Relop();
		Expr();
	}
	/**
	 * Relop = "==" | "!=" | ">" | ">=" | "<" | "<=".
	 */
	private void Relop() {
		if(sym==Token.Kind.assign || sym==Token.Kind.neq || sym==Token.Kind.geq || sym==Token.Kind.gtr || sym==Token.Kind.lss || sym==Token.Kind.leq || sym==Token.Kind.eql){
			scan();
		}else{
			error(Message.REL_OP);
		}
		
	}

	private void Statement_If() {
		scan();
		check(Token.Kind.lpar);
		Condition();
		check(Token.Kind.rpar);
		Statement();
		if(sym==Token.Kind.else_){
			scan();
			Statement();
		}
	}
	/**
	 * ClassDecl = "class" ident "{" { VarDecl } "}".
	 */
	private void ClassDecl() {
		check(Token.Kind.class_);
		check(Token.Kind.ident);
		Obj myClass = tab.insert(Obj.Kind.Type, t.str, new Struct(Struct.Kind.Class));
		check(Token.Kind.lbrace);
		tab.openScope();
		while(VarDecl.hasFirst(sym)){
			VarDecl();
		}
		if(tab.curScope.nVars()>MAX_FIELDS)
			error(Message.TOO_MANY_FIELDS);
		check(Token.Kind.rbrace);
		
		myClass.type.fields = tab.curScope.locals();
		tab.closeScope();
	}
	/**
	 * VarDecl = Type ident { "," ident } ";".
	 */
	private void VarDecl() {
		Struct type = Type();
		check(Token.Kind.ident);
		tab.insert(Obj.Kind.Var, t.str, type);
		while(sym==Token.Kind.comma){
			scan();
			check(Token.Kind.ident);
			tab.insert(Obj.Kind.Var, t.str, type);
		}
		check(Token.Kind.semicolon);
	}
	
	
	/**
	 * ConstDecl = "final" Type ident "=" ( number | charConst ) ";".
	 */
	private void ConstDecl(){
		check(Token.Kind.final_);
		
		Struct type = Type();
		check(Token.Kind.ident);
		Obj myObj = tab.insert(Obj.Kind.Con, t.str, type);
		
		check(Token.Kind.assign);
		if(sym == Token.Kind.number){// || sym == Token.Kind.charConst){
			if(type.kind!=Struct.Kind.Int)
				error(Message.CONST_TYPE);
			scan();
			myObj.val=t.val;
		}else if(sym == Token.Kind.charConst){// || sym == Token.Kind.charConst){
			if(type.kind!=Struct.Kind.Char)
				error(Message.CONST_TYPE);
			scan();
			myObj.val=t.val;
		}else{
			error(Message.CONST_DECL);
		}
		check(Token.Kind.semicolon);
	}
	/**
	 * Type = ident [ "[" "]" ].
	 */
	private Struct Type() {
		check(Token.Kind.ident);
		Obj o = tab.find(t.str);
		
		//if is null the type is not found
		if(o==null){
			error(Message.NOT_FOUND,t.str);
			return new Struct(Struct.Kind.None);
		}//if the type found is not a type
		else if (o.kind != Obj.Kind.Type) {
			error(Message.NO_TYPE);
			return new Struct(Struct.Kind.None);
		}
		
		Struct type = o.type;
		if(sym==Token.Kind.lbrack){
			scan();
			check(Token.Kind.rbrack);
			type = new Struct(type);
		}
		return type;
	}

	/**
	 * Starts the analysis.
	 */
	public void parse() {
		scan();
		Program();
		check(Token.Kind.eof);
	}
}
