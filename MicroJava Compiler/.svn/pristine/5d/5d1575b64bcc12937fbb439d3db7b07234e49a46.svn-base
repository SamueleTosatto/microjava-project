package ssw.mj;

import static ssw.mj.Token.Kind.none;
import ssw.mj.Errors.Message;
import ssw.mj.Token.Kind;
import ssw.mj.codegen.Code;
import ssw.mj.symtab.Tab;

/**
 * The Parser for the MicroJava Compiler.
 */
public class Parser {

	/** Maximum number of global variables per program */
	@SuppressWarnings("unused")
	private static final int MAX_GLOBALS = 32767;

	/** Maximum number of fields per class */
	@SuppressWarnings("unused")
	private static final int MAX_FIELDS = 32767;

	/** Maximum number of local variables per method */
	@SuppressWarnings("unused")
	private static final int MAX_LOCALS = 127;

	/** Last recognized token; */
	@SuppressWarnings("unused")
	private Token t;

	/** Lookahead token (not recognized).) */
	private Token la;

	/** Shortcut to kind attribute of lookahead token (la). */
	@SuppressWarnings("unused")
	private Token.Kind sym;

	/** According scanner */
	public final Scanner scanner;

	/** According code buffer */
	public final Code code;

	/** According symbol table */
	public final Tab tab;

	// TODO Exercise 3 - 6: implementation of parser
	/**
	 * This class provide an abstraction on the property(ies maybe in the future) about a Non Terminal
	 * @author Samuele
	 *
	 */
	private static class NonTerminal{
		/**
		 * This method just check if a token t is inside a token list 
		 * @param s is a list (view better as a set) fo possible token
		 * @param t	is a token
		 * @return true if t is in s, false otherwise
		 */
		protected  static boolean isIn(Token.Kind[] s, Token.Kind t){
			for(Token.Kind x : s){
				if(x==t)
					return true;
			}
			return false;
		}
	}

	private static class ConstDecl extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.final_};
		public static  boolean hasFirst( Token.Kind k) {
			return isIn(first,k);
		}
	}
	
	private static class VarDecl extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.ident/*from Type*/};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
	}
	private static class ClassDecl extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.class_};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
	}
	private static class MethodDecl extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.void_,Token.Kind.ident/*from Type*/};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
	}
	private static class Type extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.ident};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
	}
	private static class Block extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.lbrace};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
	}

	private static class Statement extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.ident/*Designator*/,Token.Kind.if_,Token.Kind.while_,Token.Kind.break_,Token.Kind.read,Token.Kind.return_,Token.Kind.print,Token.Kind.semicolon,Token.Kind.lbrace/*from Block*/};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
	}
	private static class AssignOp extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.assign,Token.Kind.plusas,Token.Kind.minusas,Token.Kind.timesas,Token.Kind.slashas,Token.Kind.remas};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
	}
	private static class ActPars extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.lpar};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
	}
	private static class Condition extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.minus,Token.Kind.ident/*Designator*/, Token.Kind.number,Token.Kind.charConst,Token.Kind.new_,Token.Kind.lpar};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
	}
	private static class CondTerm extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.minus,Token.Kind.ident/*Designator*/, Token.Kind.number,Token.Kind.charConst,Token.Kind.new_,Token.Kind.lpar};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
	}
	private static class CondFact extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.minus,Token.Kind.ident/*Designator*/, Token.Kind.number,Token.Kind.charConst,Token.Kind.new_,Token.Kind.lpar};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
	}
	private static class Expr extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.minus,Token.Kind.ident/*Designator*/, Token.Kind.number,Token.Kind.charConst,Token.Kind.new_,Token.Kind.lpar};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
	}
	private static class Term extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.ident/*Designator*/, Token.Kind.number,Token.Kind.charConst,Token.Kind.new_,Token.Kind.lpar};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
	}
	private static class Factor extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.ident/*Designator*/, Token.Kind.number,Token.Kind.charConst,Token.Kind.new_,Token.Kind.lpar};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
	}
	private static class Designator extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.ident};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
	}
	private static class AddOp extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.plus,Token.Kind.minus};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
	}
	private static class MulOp extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.times,Token.Kind.slash,Token.Kind.rem,Token.Kind.ttimes,Token.Kind.bfslash};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
	}
	private static class FromPars extends Parser.NonTerminal{
		static Token.Kind[] first = new Token.Kind[]{Token.Kind.ident};
		public static boolean hasFirst(Token.Kind k) {
			return isIn(first,k);
		}
	}
	/**
	 * Initialization of parser. Need a scanner as parameter
	 * @param scanner
	 */
	public Parser(Scanner scanner) {
		this.scanner = scanner;
		tab = new Tab(this);
		
		// Avoid crash when 1st symbol has scanner error.
		la = new Token(none, 1, 1);
		code = new Code(this);
	}

	/**
	 * Adds error message to the list of errors.
	 */
	public void error(Message msg, Object... msgParams) {
		scanner.errors.error(la.line, la.col, msg, msgParams);
		// panic mode
		throw new Errors.PanicMode();
	}
	/**
	 * This method retrive the next token from the scanner
	 */
	private void scan () {
		 t = la; 
		 la = scanner.next(); 
		 sym = la.kind;
	}
	/**
	 * This check the token we expect is the same as the actual one.
	 * If is true then we just scan the new token, we throw an error otherwise. 
	 * @param expected
	 */
	private void check (Token.Kind expected) {
		 if (sym == expected) {
			 scan();
		 } else {
			 error(Message.TOKEN_EXPECTED, expected);
		 }
	}
	/**
	 *Program =  "program" ident { ConstDecl | VarDecl | ClassDecl }
"{" {MethodDecl} "}"
	 */
	private void Program () {
		/*"program"*/
		check(Token.Kind.program);
		/*ident*/
		check(Token.Kind.ident);
		/*{ ConstDecl | VarDecl | ClassDecl }*/
		while(ConstDecl.hasFirst(sym) || VarDecl.hasFirst(sym) || ClassDecl.hasFirst(sym)){
			if(ConstDecl.hasFirst(sym)){
				ConstDecl();
			}else if(VarDecl.hasFirst(sym)){
				VarDecl();
			}else if(ClassDecl.hasFirst(sym)){
				ClassDecl();
			}
		}
		/*"{"*/
		check(Token.Kind.lbrace);
		/*{ MethodDecl }*/
		while(MethodDecl.hasFirst(sym)){
			MethodDecl();
		}
		/*}*/
		check(Token.Kind.rbrace);
	}
	/**MethodDecl = ( Type | "void" ) ident "(" [ FormPars ] ")"
{ VarDecl } Block.*/
	private void MethodDecl() {
		//( Type | "void" )
		if(Type.hasFirst(sym)){
			Type();
		}else if(sym == Token.Kind.void_){
			scan();
		}else{
			error(Message.METH_DECL);
		}
		//ident
		check(Token.Kind.ident);
		//"("
		check(Token.Kind.lpar);
		//[ FormPars ] 
		if(FromPars.hasFirst(sym)){
			FormPars();
		}
		//")"
		check(Token.Kind.rpar);
		//{ VarDecl }
		while(VarDecl.hasFirst(sym)){
			VarDecl();
		}
		//Block
		Block();
	}
	/**
	 * FormPars = Type ident { "," Type ident }.
	 */
	private void FormPars() {
		//Type
		Type();
		//ident
		check(Token.Kind.ident);
		//{"," Type ident
		while(sym==Token.Kind.comma){
			scan();
			Type();
			check(Token.Kind.ident);
		}
		
	}
	/**
	 * Block = "{" { Statement } "}".
	 * 
	 */
	private void Block() {
		//"("
		check(Token.Kind.lbrace);
		//{Statement}
		while(Statement.hasFirst(sym)){
			Statement();
		}
		//")"
		check(Token.Kind.rbrace);
	}
	
	private void Statement_Designator(){
		Designator();
		if(AssignOp.hasFirst(sym)){
			AssignOp();
			Expr();
		}else if(ActPars.hasFirst(sym)){
			ActPars();
		}else if(sym==Token.Kind.pplus){
			scan();
		}else if(sym==Token.Kind.mminus){
			scan();
		}else{
			error(Message.DESIGN_FOLLOW);
		}
		check(Token.Kind.semicolon);
	}
	/**
	 * ActPars = "(" [ Expr { "," Expr } ] ")"
	 */
	private void ActPars() {
		//"("
		check(Token.Kind.lpar);
		//[
		if(Expr.hasFirst(sym)){
			//Expr
			Expr();
			//{"," Expr}
			while(sym==Token.Kind.comma){
				scan();
				Expr();
			}
		}//]
		//")"
		check(Token.Kind.rpar);
		
	}
	/**
	 * Expr = [ "–" ] Term { Addop Term }.
	 */
	private void Expr() {
		if(sym==Token.Kind.minus){
			scan();
		}
		Term();
		while(AddOp.hasFirst(sym)){
			AddOp();
			Term();
		}
		
	}
	/**
	 * Addop = "+" | "–".
	 */
	private void AddOp() {
		if(sym==Token.Kind.plus){
			scan();
		}else if(sym==Token.Kind.minus){
			scan();
		}else{
			error(Message.ADD_OP);
		}
		
	}
	/**
	 * Term = Factor { Mulop Factor }.
	 */
	private void Term() {
		//factor
		Factor();
		//{Mulop Factor}
		while(MulOp.hasFirst(sym)){
			MulOp();
			Factor();
		}
		
	}
	/**
	 * Mulop = "*" | "/" | "%" | "**" | "\/".
	 */
	private void MulOp() {
		if(sym==Token.Kind.times){
			scan();
		}else if(sym==Token.Kind.slash){
			scan();
		}else if(sym==Token.Kind.rem){
			scan();
		}else if(sym==Token.Kind.ttimes){
			scan();
		}else if(sym==Token.Kind.bfslash){
			scan();
		}else{
			error(Message.MUL_OP);
		}
		
	}
	/**
	 * Factor = Designator [ ActPars ]
			| number
			| charConst
			| "new" ident [ "[" Expr "]" ]
			| "(" Expr ")".
	 */
	private void Factor() {
		//Designator [ ActPars ]
		if(Designator.hasFirst(sym)){//posso rimuovere
			Designator();
			if(ActPars.hasFirst(sym)){
				ActPars();
			}
			//number
		}else if(sym==Token.Kind.number){
			scan();
			//charConst
		}else if(sym==Token.Kind.charConst){
			scan();
			//new ident [ "[" Expr "]" ]
		}else if(sym==Token.Kind.new_){
			scan();
			check(Token.Kind.ident);
			if(sym==Token.Kind.lbrack){
				scan();
				Expr();
				check(Token.Kind.rbrack);
			}//"(" Expr ")".
		}else if(sym==Token.Kind.lpar){
			scan();
			Expr();
			check(Token.Kind.rpar);
			//else... Error
		}else{
			error(Message.INVALID_FACT);
		}
		
	}
	/**
	 * Assignop = "=" | "+=" | "-=" | "*=" | "/=" | "%=".
	 */
	private void AssignOp() {
		if(sym==Token.Kind.timesas){
			scan();
		}else if(sym==Token.Kind.slashas){
			scan();
		}else if(sym==Token.Kind.remas){
			scan();
		}else if(sym==Token.Kind.assign){
			scan();
		}else if(sym==Token.Kind.minusas){
			scan();
		}else if(sym==Token.Kind.plusas){
			scan();
		}else{
			error(Message.ASSIGN_OP);
		}
	}
	/**
	 * Designator = ident { "." ident | "[" Expr "]" }.
	 */
	private void Designator() {
		//ident
		check(Token.Kind.ident);
		//{
		while(sym==Token.Kind.period|| sym==Token.Kind.lbrack){
			//"[" expr "]"
			if(sym==Token.Kind.lbrack){
				scan();
				Expr();
				check(Token.Kind.rbrack);
				//"." ident
			}else if(sym==Token.Kind.period){
				scan();
				check(Token.Kind.ident);
			}
		}//}
		
	}
	/**
	 * Statement = Designator ( Assignop Expr | ActPars | "++" | "--" ) ";"
					| "if" "(" Condition ")" Statement [ "else" Statement ]
					| "while" "(" Condition ")" Statement
					| "break" ";"
					| "return" [ Expr ] ";"
					| "read" "(" Designator ")" ";"
					| "print" "(" Expr [ "," number ] ")" ";"
					| Block
					| ";".
	 */
	private void Statement() {
		// Designator ( Assignop Expr | ActPars | "++" | "--" ) ";"
		if(Designator.hasFirst(sym)){
			Statement_Designator();
			//"if" "(" Condition ")" Statement [ "else" Statement ]
		}else if(sym==Token.Kind.if_){
			Statement_If();
			//"while" "(" Condition ")" Statement
		}else if(sym==Token.Kind.while_){
			Statement_While();
			//"break" ";"
		}else if(sym==Token.Kind.break_){
			Statement_Break();
			//"return" [ Expr ] ";"
		}else if(sym==Token.Kind.return_){
			Statement_Return();
			//"read" "(" Designator ")" ";"
		}else if(sym==Token.Kind.read){
			Statement_Read();
			//"print" "(" Expr [ "," number ] ")" ";"
		}else if(sym==Token.Kind.print){
			Statement_Print();
			//Block
		}else if(Block.hasFirst(sym)){
			Block();
			//";"
		}else if(sym==Token.Kind.semicolon){
			scan();
			//else.. Error!
		}else{
			error(Message.INVALID_STAT);
		}
		
	}

	private void Statement_Print() {
		scan();
		check(Token.Kind.lpar);
		Expr();
		if(sym==Token.Kind.comma){
			scan();
			check(Token.Kind.number);
			check(Token.Kind.rpar);
			check(Token.Kind.semicolon);
		}else{
			check(Token.Kind.rpar);
			check(Token.Kind.semicolon);
		}
	}

	private void Statement_Read() {
		scan();
		check(Token.Kind.lpar);
		Designator();
		check(Token.Kind.rpar);
		check(Token.Kind.semicolon);
	}

	private void Statement_Return() {
		scan();
		if(Expr.hasFirst(sym)){
			Expr();
			check(Token.Kind.semicolon);
		}else if(sym==Token.Kind.semicolon){
			scan();
		}else{
			error(Message.INVALID_STAT);
		}
	}

	private void Statement_Break() {
		scan();
		check(Token.Kind.semicolon);
	}

	private void Statement_While() {
		scan();
		check(Token.Kind.lpar);
		if(Condition.hasFirst(sym)){
			Condition();
		}else{
			error(Message.INVALID_STAT);
		}
		check(Token.Kind.rpar);
		Statement();
	}
	/**
	 * Condition = CondTerm { "||" CondTerm }.
	 */
	private void Condition() {
		CondTerm();
		while(sym==Token.Kind.or){
			scan();
			CondTerm();
		}
	}
	/**
	 * CondTerm = CondFact { "&&" CondFact }.
	 */
	private void CondTerm() {
		CondFact();
		while(sym==Token.Kind.and){
			scan();
			CondFact();
		}
	}
	/**
	 * CondFact = Expr Relop Expr
	 */
	private void CondFact() {
		Expr();
		Relop();
		Expr();
	}
	/**
	 * Relop = "==" | "!=" | ">" | ">=" | "<" | "<=".
	 */
	private void Relop() {
		if(sym==Token.Kind.assign || sym==Token.Kind.neq || sym==Token.Kind.geq || sym==Token.Kind.gtr || sym==Token.Kind.lss || sym==Token.Kind.leq){
			scan();
		}else{
			error(Message.REL_OP);
		}
		
	}

	private void Statement_If() {
		scan();
		check(Token.Kind.lpar);
		Condition();
		check(Token.Kind.rpar);
		Statement();
		if(sym==Token.Kind.else_){
			scan();
			Statement();
		}
	}
	/**
	 * ClassDecl = "class" ident "{" { VarDecl } "}".
	 */
	private void ClassDecl() {
		check(Token.Kind.class_);
		check(Token.Kind.ident);
		check(Token.Kind.lbrace);
		while(VarDecl.hasFirst(sym)){
			VarDecl();
		}
		check(Token.Kind.rbrace);
	}
	/**
	 * VarDecl = Type ident { "," ident } ";".
	 */
	private void VarDecl() {
		Type();
		check(Token.Kind.ident);
		while(sym==Token.Kind.comma){
			scan();
			check(Token.Kind.ident);
		}
		check(Token.Kind.semicolon);
	}
	/**
	 * ConstDecl = "final" Type ident "=" ( number | charConst ) ";".
	 */
	private void ConstDecl(){
		check(Token.Kind.final_);
		Type();
		check(Token.Kind.ident);
		check(Token.Kind.assign);
		if(sym == Token.Kind.number || sym == Token.Kind.charConst){
			scan();
		}else{
			error(Message.CONST_DECL);
		}
		check(Token.Kind.semicolon);
	}
	/**
	 * Type = ident [ "[" "]" ].
	 */
	private void Type() {
		check(Token.Kind.ident);
		if(sym==Token.Kind.lbrack){
			scan();
			check(Token.Kind.rbrack);
		}
	}

	/**
	 * Starts the analysis.
	 */
	public void parse() {
		scan();
		Program();
		check(Token.Kind.eof);
	}
}
